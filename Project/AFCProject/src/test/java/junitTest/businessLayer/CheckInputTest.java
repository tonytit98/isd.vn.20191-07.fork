package junitTest.businessLayer;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import businessLayer.CheckInput;

public class CheckInputTest {
	
	private CheckInput checkInputTest;
	@BeforeEach
	void setUp() throws Exception {
		checkInputTest = new CheckInput();
	}

	@Test
	public void testCheckEnterExit() {
		int input, result;
		input = checkInputTest.checkEnterExit("1-a");
		result = 1;
		assertEquals(result, input);
		
		input = checkInputTest.checkEnterExit("2-a");
		result = 2;
		assertEquals(result, input);
		
		input = checkInputTest.checkEnterExit("3-a");
		result = 0;
		assertEquals(result, input);
		
		input = checkInputTest.checkEnterExit("4-a");
		result = 0;
		assertEquals(result, input);
	}

	@Test
	public void testCheckInput() {
		boolean input;
		input = checkInputTest.checkInput("1-b");
		assertEquals(true, input);
		
		input = checkInputTest.checkInput("2-d");
		assertEquals(true, input);
		
		input = checkInputTest.checkInput("2d-");
		assertEquals(false, input);
	}

	@Test
	public void testCheckStation() {
		boolean input;
		input = checkInputTest.checkStation("1-a");
		assertEquals(true, input);
		
		input = checkInputTest.checkStation("2-d");
		assertEquals(true, input);
		
		input = checkInputTest.checkStation("2-i");
		assertEquals(true, input);
		
		input = checkInputTest.checkStation("1-o");
		assertEquals(false, input);
		
		input = checkInputTest.checkStation("2-m");
		assertEquals(false, input);
	}

	@Test
	public void testCheckPseudoBarCode() {
		int input, result;
		input = checkInputTest.checkPseudoBarCode("1234567");
		result = -1;
		assertEquals(result, input);
		
		input = checkInputTest.checkPseudoBarCode("ABCDEFG1");
		result = -1;
		assertEquals(result, input);
		
		input = checkInputTest.checkPseudoBarCode("ABCDEFGH");
		result = 1;
		assertEquals(result, input);
		
		input = checkInputTest.checkPseudoBarCode("abcdefgh");
		result = 2;
		assertEquals(result, input);
	}

}
