package dataLayer;

import java.sql.Connection;

import java.sql.SQLException;

/**
 * @author Do Thuy Nga
 *
 */
public class ConnectionUtils {
	 /**
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public static Connection getMyConnection() throws SQLException,ClassNotFoundException {

		 return MySQLConnUtils.getMySQLConnection();
	 }

	 
	 // Test Connection ...
	 
//	 public static void main(String[] args) throws SQLException,
//     ClassNotFoundException {
//
//		 System.out.println("Get connection ... ");
//
//		 // Lấy ra đối tượng Connection kết nối vào database.
//		 Connection conn = ConnectionUtils.getMyConnection();
//
//		 System.out.println("Get connection " + conn);
//
//		 System.out.println("Done!");
//	 }
}
