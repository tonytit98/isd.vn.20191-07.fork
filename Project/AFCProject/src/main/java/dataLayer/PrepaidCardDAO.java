package dataLayer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dataTransferObject.PrepaidCard;

/**
 * @author Vu Duc Nguyen
 * email: ducnguyen272727@gmail.com
 *
 */
public class PrepaidCardDAO {
	PrepaidCard card;
	private static PrepaidCardDAO instance;
	private PrepaidCardDAO(){}
	
	public static PrepaidCardDAO getInstance(){
		if(instance == null){
            instance = new PrepaidCardDAO();
        }
        return instance;
	}
	
	/**
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<PrepaidCard> getAllPrepaidCard() throws ClassNotFoundException, SQLException {
		ArrayList<PrepaidCard> allCards = new ArrayList<PrepaidCard>();
		String sql = "select * from prepaidcard";
		
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {// Di chuyá»ƒn con trá»� xuá»‘ng báº£n ghi káº¿ tiáº¿p.
        	card = new PrepaidCard();
        	card.setId(rs.getString("Id"));
        	card.setBalance(rs.getDouble("Balance"));
        	card.setCode(rs.getString("Code"));
        	allCards.add(card);
        }
		connection.close();
		return allCards;
	}
	
	// Láº¥y thÃ´ng tin cá»§a card bang id
	/**
	 * @param id
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public PrepaidCard getCardById(String id) throws ClassNotFoundException, SQLException {
		card = new PrepaidCard();
		String sql = "SELECT * from prepaidcard WHERE Id="+"\""+id +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
        	card.setId(rs.getString("Id"));
        	card.setBalance(rs.getDouble("Balance"));
        	card.setCode(rs.getString("Code"));
        }
		connection.close();
		return card;
	}
	
	// Láº¥y thÃ´ng tin cá»§a card bang code
		/**
		 * @param code
		 * @return
		 * @throws ClassNotFoundException
		 * @throws SQLException
		 */
		public PrepaidCard getCardByCode(String code) throws ClassNotFoundException, SQLException {
			card = new PrepaidCard();
			String sql = "SELECT * from prepaidcard WHERE Code="+"\""+code +"\"";
			Connection connection = ConnectionUtils.getMyConnection();
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			
			while (rs.next()) {
	        	card.setId(rs.getString("Id"));
	        	card.setBalance(rs.getDouble("Balance"));
	        	card.setCode(rs.getString("Code"));
	        }
			connection.close();
			return card;
		}
	/**
	 * @param id
	 * @param balance
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean updateBalance(String id, double balance) throws ClassNotFoundException, SQLException {
		String sql = "UPDATE prepaidcard set Balance="+balance+" WHERE Id="+"\""+id +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		boolean check = statement.execute(sql);
		return check;
	}
}
