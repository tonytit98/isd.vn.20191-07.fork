package businessLayer.card;

import java.sql.SQLException;

import hust.soict.se.customexception.InvalidIDException;

/**
 * @author Vu Duc Nguyen
 *
 */
public interface PrepaidCardInterface {
	/**
	 * @param id
	 * @param calculateFee
	 * @return
	 */
	public double updateBalance(String id, double calculateFee);
	/**
	 * @param pseudoBarCode
	 * @param stationID
	 * @throws InvalidIDException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void handleCardWhenEnterStation(String pseudoBarCode, int stationID) throws InvalidIDException, ClassNotFoundException, SQLException;
	/**
	 * @param pseudoBarCode
	 * @param stationID
	 * @throws InvalidIDException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void handleCardWhenExitStation(String pseudoBarCode, int stationID) throws InvalidIDException, ClassNotFoundException, SQLException;
}
