package businessLayer.ticket;

import java.sql.SQLException;

import hust.soict.se.customexception.InvalidIDException;

/**
 * @author Admin
 *
 */
public interface TicketInterface {
	/**
	 * @param code
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static int getTicketType(String code) throws ClassNotFoundException, SQLException {
		return 0;
	}
	
	/**
	 * @param pseudoBarCode
	 * @param stationID
	 * @throws InvalidIDException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws NullPointerException
	 */
	public static void handleTicketWhenEnterStation(String pseudoBarCode,int stationID) 
			throws InvalidIDException, ClassNotFoundException, SQLException, NullPointerException {
	}
	
	/**
	 * @param pseudoBarCode
	 * @param stationID
	 * @throws InvalidIDException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void handleTicketWhenExitStation(String pseudoBarCode,int stationID) throws InvalidIDException, ClassNotFoundException, SQLException {
	}
}
