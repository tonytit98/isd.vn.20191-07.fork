package businessLayer.ticket;


import dataTransferObject.OnewayTicket;


/**
 * @author Admin
 *
 */
public interface OnewayTicketInterface {
	/**
	 * @param id
	 * @return
	 */
	public static OnewayTicket getTicketById(String id){
		return null;
	}
	/**
	 * @param code
	 * @return
	 */
	public static OnewayTicket getTicketByCode(String code){
		return null;
	}
	/**
	 * @param StationID
	 * @param owTicket
	 * @return
	 */
	public static int checkEmbarkation(int StationID,OnewayTicket owTicket) {
		return 0;
	}
	/**
	 * @param stationID
	 * @param owTicket
	 * @return
	 */
	public static int checkDisembarkation(int stationID,OnewayTicket owTicket) {
		return 0;
	}
	/**
	 * @param pseudoBarCode
	 * @param stationID
	 */
	public static void handleOnewayTicketWhenEnterStation(String pseudoBarCode, int stationID){
	}
	/**
	 * @param pseudoBarCode
	 * @param stationID
	 */
	public static void handleOnewayTicketWhenExitStation(String pseudoBarCode, int stationID){
	}
}
