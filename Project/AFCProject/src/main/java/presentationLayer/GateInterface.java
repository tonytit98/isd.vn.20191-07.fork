package presentationLayer;

import hust.soict.se.gate.Gate;

/**
 * @author Vu Duc Nguyen
 *
 */
public class GateInterface {
	public static void open() {
		Gate gate = Gate.getInstance();
		gate.open();
	}
	
	public static void close() {
		Gate gate = Gate.getInstance();
		gate.close();
	}
}
