package junitTest.businessLayer.card;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import businessLayer.card.CardBUS;

public class CardBUSTest {
	
	private CardBUS cardBUSTest; 
	@BeforeEach
	void setUp() throws Exception {
		cardBUSTest = new CardBUS();
	}

	@Test
	public void testCheckBalanceFirst() {
		boolean input;
		input = cardBUSTest.checkBalanceFirst("9ac2197d9258257b");
		assertEquals(false, input);
		
		input = cardBUSTest.checkBalanceFirst("ABCDEFGH");
		assertEquals(false, input);
		
		input = cardBUSTest.checkBalanceFirst("7bf52afd1d2eb936");
		assertEquals(false, input);
	}

	@Test
	public void testCalculateFee() throws ClassNotFoundException, SQLException {
		double input, result;
		input = cardBUSTest.calculateFee(1, 2);
		result = 1.9;
		
		input = cardBUSTest.calculateFee(1, 3);
		result = 2.7;
		
		input = cardBUSTest.calculateFee(2, 5);
		result = 4.3;
		
		input = cardBUSTest.calculateFee(7,9);
		result = 3.5;
	}

}
