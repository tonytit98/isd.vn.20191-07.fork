package junitTest.businessLayer.ticket;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import dataLayer.TwentyfourHoursTicketDAO;

public class TwentyfourHoursTicketBUSTest {

	@Test
	public void testGetTicketById() throws ClassNotFoundException, SQLException {
		String id="TF201910100293";
		assertNotEquals(TwentyfourHoursTicketDAO.findbyId(id), null);
	}

	@Test
	public void testGetTicketByCode() throws ClassNotFoundException, SQLException {
		String code="07c84c6c4ba59f88";
		assertNotEquals(TwentyfourHoursTicketDAO.getByCode(code), null);
	}

	@Test
	public void testCheckTFActive() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTFExpireTime() {
		fail("Not yet implemented");
	}

	@Test
	public void testCheckValidTFTicket() {
		fail("Not yet implemented");
	}

	@Test
	public void testDisplayTfhTicketInfo() {
		fail("Not yet implemented");
	}

	@Test
	public void testDisplayInvalidTfhTicketInfo() {
		fail("Not yet implemented");
	}

	@Test
	public void testDisplayExpiredMessage() {
		fail("Not yet implemented");
	}

	@Test
	public void testProcessTfhTicketExit() {
		fail("Not yet implemented");
	}

	@Test
	public void testProcessTfhTicket() {
		fail("Not yet implemented");
	}

}
