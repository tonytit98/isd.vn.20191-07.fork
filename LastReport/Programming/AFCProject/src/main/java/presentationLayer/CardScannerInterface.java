package presentationLayer;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.scanner.CardScanner;

/**
 * @author Vu Duc Nguyen
 *
 */
public class CardScannerInterface {
	public static String process(String pseudoBarCode) throws InvalidIDException {
		CardScanner cardScanner = CardScanner.getInstance();
		return cardScanner.process(pseudoBarCode);
	}
}