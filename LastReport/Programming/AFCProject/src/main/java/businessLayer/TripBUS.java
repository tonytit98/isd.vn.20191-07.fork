package businessLayer;

import java.sql.SQLException;
import java.util.ArrayList;

import dataLayer.TripDAO;
import dataTransferObject.Trip;

/**
 * @author Vu Duc Nguyen
 * email: ducnguyen272727@gmail.com
 *
 */
public class TripBUS {
	
	private static TripBUS instance;
	private TripBUS(){}
	
	public static TripBUS getInstance(){
		if(instance == null){
            instance = new TripBUS();
        }
        return instance;
	}
	
	static TripDAO tripDAO = TripDAO.getInstance();
	
	/**
	 * @param idTicketCard
	 * @param departureStationID
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static int createNewTrip(String idTicketCard, int departureStationID) throws ClassNotFoundException, SQLException {
		return tripDAO.createNewTrip(idTicketCard, departureStationID);
	}
	
	/**
	 * @param id
	 * @param arrivalStationID
	 * @param price
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static int updateTrip(int id, int arrivalStationID, double price) throws ClassNotFoundException, SQLException {
		return tripDAO.updateTrip(id, arrivalStationID, price);
	}
	
	/**
	 * @param ticketCardID
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static ArrayList<Trip> getTripByTicketCardID (String ticketCardID) throws ClassNotFoundException, SQLException {
		return tripDAO.getTripByTicketCardID(ticketCardID);
	}
	
	/**
	 * @param id
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static Trip getTripById(Integer id) throws ClassNotFoundException, SQLException {
		return tripDAO.getTripById(id);
	}
}
