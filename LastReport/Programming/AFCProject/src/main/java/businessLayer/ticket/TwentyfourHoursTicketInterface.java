package businessLayer.ticket;

import java.sql.SQLException;
import java.sql.Timestamp;

import dataTransferObject.TwentyfourHoursTicket;

/**
 * @author Admin
 *
 */
public interface TwentyfourHoursTicketInterface {
	/**
	 * @param id
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public TwentyfourHoursTicket getTicketById(String id) throws ClassNotFoundException, SQLException;
	
	/**
	 * @param code
	 * @return
	 */
	public static TwentyfourHoursTicket getTicketByCode(String code) {
		return null;
	}
	/**
	 * @param tfhTicket
	 * @return
	 */
	public static int checkTFActive(TwentyfourHoursTicket tfhTicket) {
		return 0;
	}
	/**
	 * @param tfhTicket
	 * @return
	 */
	public static Timestamp getTFExpireTime(TwentyfourHoursTicket tfhTicket) {
		return null;
	}
	/**
	 * @param tfhTicket
	 * @return
	 */
	public static int checkValidTFTicket(TwentyfourHoursTicket tfhTicket) {
		return 0;
	}
	/**
	 * @param tfhTicket
	 */
	public static void displayTfhTicketInfo(TwentyfourHoursTicket tfhTicket) {
	}
	/**
	 * @param tfhTicket
	 */
	public static void displayInvalidTfhTicketInfo(TwentyfourHoursTicket tfhTicket) {
	}
	/**
	 * @param tfhTicket
	 */
	public static void displayExpiredMessage(TwentyfourHoursTicket tfhTicket) {
	}
	/**
	 * @param pseudoBarCode
	 */
	public static void processTfhTicketExit(String pseudoBarCode) {
	}
	/**
	 * @param pseudoBarCode
	 */
	public static void processTfhTicket(String pseudoBarCode) {
	}
}
