package dataLayer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dataTransferObject.Trip;

/**
 * @author Vu Duc Nguyen
 * email: ducnguyen272727@gmail.com
 *
 */
public class TripDAO {
	
	private static TripDAO instance;
	private TripDAO(){}
	
	public static TripDAO getInstance(){
		if(instance == null){
            instance = new TripDAO();
        }
        return instance;
	}
	
	/**
	 * @param ticketCardID
	 * @param departureStationID
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int createNewTrip(String ticketCardID, int departureStationID) throws ClassNotFoundException, SQLException {
		String sql = "INSERT INTO trip(ticketCardID, departureStationID, departureTime, status) VALUES ('"+
				ticketCardID + "'," + departureStationID+", NOW(), 1)";
		Connection connection = ConnectionUtils.getMyConnection();
		
		Statement statement = connection.createStatement();
		int rs = statement.executeUpdate(sql);

		connection.close();
		return rs;
	}
	
	/**
	 * @param id
	 * @param arrivalStationID
	 * @param price
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int updateTrip(int id, int arrivalStationID, double price) throws ClassNotFoundException, SQLException {
		String sql = "UPDATE trip set arrivalStationID ="+arrivalStationID+",arrivalTime = NOW()" + ", status = 0 "+
				", price =" + price +" where id ="+ id + " AND status = 1";
		Connection connection = ConnectionUtils.getMyConnection();
		
		Statement statement = connection.createStatement();
		int rs = statement.executeUpdate(sql);

		connection.close();
		return rs;
	}
	
	// status = 1
	/**
	 * @param ticketCardID
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<Trip> getTripByTicketCardID (String ticketCardID) throws ClassNotFoundException, SQLException {
		ArrayList<Trip> trips = new ArrayList<Trip>();
		String sql = "select * from trip where status = 1 AND ticketCardID = '" + ticketCardID + "'";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
        	Trip trip = new Trip();
        	trip.setId(rs.getInt("id"));
        	trip.setTicketCardID(rs.getString("ticketCardID"));
        	trip.setDepartureStationID(rs.getInt("departureStationID"));
        	trip.setDepartureTime(rs.getDate("departureTime"));
        	trip.setArrivalStationID(rs.getInt("arrivalStationID"));
        	trip.setArrivalTime(rs.getDate("arrivalTime"));
        	trip.setStatus(rs.getInt("status"));
        	trip.setPrice(rs.getDouble("price"));
        	trips.add(trip);
        }
		connection.close();
		return trips;
	}
	
	
	/**
	 * @param id
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public Trip getTripById(Integer id) throws ClassNotFoundException, SQLException {
		String sql = "select * from trip where status = 1 AND id = '" + id + "'";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		Trip trip = new Trip();
		while (rs.next()) {
        	trip.setId(rs.getInt("id"));
        	trip.setTicketCardID(rs.getString("ticketCardID"));
        	trip.setDepartureStationID(rs.getInt("departureStationID"));
        	trip.setDepartureTime(rs.getDate("departureTime"));
        	trip.setArrivalStationID(rs.getInt("arrivalStationID"));
        	trip.setArrivalTime(rs.getDate("arrivalTime"));
        	trip.setStatus(rs.getInt("status"));
        	trip.setPrice(rs.getDouble("price"));
        }
		connection.close();
		return trip;
	}
}
