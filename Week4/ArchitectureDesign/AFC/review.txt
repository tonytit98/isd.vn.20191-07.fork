ISD.VN.20191-07
Tổng hợp review
Homework week4
---------------
Reviewer: Nguyễn Trọng Nghĩa
review for: Đỗ Thúy Nga
Content:
1.  Nghĩa nghĩ là Nga nên thêm những trường hợp mà vé bị invalid thì
  trường hợp đấy xảy ra sẽ có chuyện gì.
2.  Theo t bản thân của Ticket Regcognizer chỉ là để đọc mã vé và r
  hệ thống sẽ kiểm tra xem cái vé đấy có hợp lệ hay không chứ không
  phải là nó có thể kiểm tra được luôn đâu.
---------------
Reviewer:Đỗ Thúy Nga
review for: Nguyễn Đình Minh
Content:
-Ảnh nên thêm phần góc trên 1 chút nữa
-Khoảng cách giữa các phần nên cân đối và có đủ khoảng cách để nhìn rõ chữ
-Nên thêm phần check xem có đúng là prepaid card hay không
---------------
Reviewer: Vũ Đức Nguyễn
review for: Nguyễn Trọng Nghĩa
Content:
- Về bài exit using one-way ticket
  + Để là Database quá chung chung, nên để là 1 cái gì đó cụ thể.
  + Thiếu actor: Gate, Displayer.
  + Thiếu check nhà ga hành khách nên xem có nằm giữa 2 nhà ga trên vé không.
  + Ticket Recognizer là 1 actor chứ ko phải đường bao.
- Về bài enter using 24h ticket.
  + Để là Database quá chung chung, nên để là 1 cái gì đó cụ thể.
  + Thiếu actor: Gate, Displayer.
---------------
Reviewer: Nguyễn Đình Minh
review for: Vũ Đức Nguyễn
Content:
- Rename ảnh cho dễ hiểu.
- Đặt tên cho diagrams trong asta (sau chữ sd in đậm ở góc trái trên)
Về exit using 24h
1.  Nên thay TicketAccount bằng Database, hệ thống thực tế không lưu bằng TicketAccount
2.  DisplayInterface = DisplayerInterface
3.  Nên thêm phần thông báo message tình trạng vé cho hành khách.
4.  Chưa mô tả khi vé không hợp lệ.
Về enter using card
1.  Nên thay CardAccount bằng Database, hệ thống thực tế không lưu bằng CardAccount
2.  Ảnh không bao quát được hết file.
3.  Chưa mô tả ở 1.2.2 và 1.2.2.1 khi chưa hợp lệ
