# Work assignment in AFC: #
* Leader: Nguyễn Đình Minh
## Đỗ Thúy Nga ##
### Design flow of events for 2 use case :
  1. Scan ticket
  2. Check information of prepaid card when enter station.

## Nguyễn Đình Minh ##
### Design flow of events for 2 use case :
  1. Check information of prepaid card when enter station.
  2. Check information of prepaid card when exit station.

## Nguyễn Trọng Nghĩa ##
### Design flow of events for 2 use case:
  1. Control gate.
  2. Display information of ticket/card.

## Vũ Đức Nguyễn ##
### Thiết kế luồng sự kiện cho 2 use case:
  1. Scan card.
  2. Check information of one-way tickets when enter station.
