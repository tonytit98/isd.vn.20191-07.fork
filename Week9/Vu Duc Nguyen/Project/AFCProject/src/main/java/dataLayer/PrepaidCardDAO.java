package dataLayer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dataTransferObject.PrepaidCard;

public class PrepaidCardDAO {
	public static ArrayList<PrepaidCard> getAllPrepaidCard() throws ClassNotFoundException, SQLException {
		ArrayList<PrepaidCard> allCards = new ArrayList<PrepaidCard>();
		String sql = "select * from prepaidcard";
		
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {// Di chuyển con trỏ xuống bản ghi kế tiếp.
        	PrepaidCard card = new PrepaidCard();
        	card.setId(rs.getString("Id"));
        	card.setBalance(rs.getDouble("Balance"));
        	card.setCode(rs.getString("Code"));
        	allCards.add(card);
        }
		connection.close();
		return allCards;
	}
	
	// Lấy thông tin của card bang id
	public static PrepaidCard getCardById(String id) throws ClassNotFoundException, SQLException {
		PrepaidCard card = new PrepaidCard();
		String sql = "SELECT * from prepaidcard WHERE Id="+"\""+id +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
        	card.setId(rs.getString("Id"));
        	card.setBalance(rs.getDouble("Balance"));
        	card.setCode(rs.getString("Code"));
        }
		connection.close();
		return card;
	}
	
	// Lấy thông tin của card bang code
		public static PrepaidCard getCardByCode(String code) throws ClassNotFoundException, SQLException {
			PrepaidCard card = new PrepaidCard();
			String sql = "SELECT * from prepaidcard WHERE Code="+"\""+code +"\"";
			Connection connection = ConnectionUtils.getMyConnection();
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			
			while (rs.next()) {
	        	card.setId(rs.getString("Id"));
	        	card.setBalance(rs.getDouble("Balance"));
	        	card.setCode(rs.getString("Code"));
	        }
			connection.close();
			return card;
		}
	public static boolean updateBalance(String id, double balance) throws ClassNotFoundException, SQLException {
		String sql = "UPDATE prepaidcard set Balance="+balance+" WHERE Id="+"\""+id +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		boolean check = statement.execute(sql);
		return check;
	}

}
