package presentationLayer;

import businessLayer.*;
import dataLayer.TripDAO;
import dataTransferObject.*;

import java.sql.SQLException;
import java.util.Scanner;

import hust.soict.se.customexception.InvalidIDException;

public class MainGUI {
	public static String mainScreen() {
		System.out.println("These are stations in the line M14 of Paris");
		System.out.print("a. Saint-Lazare\n" +
				"b. Madeleine\n" + 
				"c. Pyramides\n" + 
				"d. Chatelet\n" + 
				"e. Gare de Lyon\n" + 
				"f. Bercy\n" + 
				"g. Cour Saint-Emilion\n" + 
				"h. Bibliotheque Francois Mitterrand\n" + 
				"i. Olympiades\n");
		System.out.println("Available actions: 1-enter station, 2-exit station");
		System.out.println("You can provide a combination of number (1 or 2) \n"
				+ "and a letter from (a to i) to enter or exit a station (using hyphen in between).\n"
				+ "For instance, the combination “2-d” will bring you to exit the station Chatelet.");
		System.out.print("Your input: ");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		return input;
	}
	
	public static int checkEnterExit(String inputMainScreen) {
		if (inputMainScreen.charAt(0) == '1') {
			return 1;
		} else if (inputMainScreen.charAt(0) == '2') {
			return 2;
		} else {
			return 0;
		}
	}
	
	// kiem tra ki tu '-'
	public static boolean checkInput(String inputMainScreen) {
		if(inputMainScreen.charAt(1) == '-') {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean checkStation(String inputMainScreen) {
		if (inputMainScreen.charAt(2) >= 'a' && inputMainScreen.charAt(2) <= 'i') {
			return true;
		} else {
			return false;
		}
	}
	
	public static String saveStation(char c) {
		if ( c=='a') {
			return "Saint-Lazare";
		} else if (c == 'b') {
			return "Madeleine";
		} else if (c == 'c') {
			return "Pyramides";
		} else if (c == 'd') {
			return "Chatelet";
		} else if (c == 'e') {
			return "Gare de Lyon";
		} else if (c == 'f') {
			return "Bercy";
		} else if (c == 'g') {
			return "Cour Saint-Emilion";
		} else if (c == 'h') {
			return "Bibliotheque Francois Mitterrand";
		} else if (c == 'i') {
			return "Olympiades";
		}
		return "error";
	}
	
	public static void clrscr() {
		// todo
	}
	
	public static void pressAnyKeyToContinue()
	 { 
		System.out.println("Press Enter key to continue...");
        try {
            System.in.read();
        } catch(Exception e) {
        	
        }
	 }
	
	public static String choosingATicketCard() {
		clrscr();
		System.out.println("Choosing a ticket/card:");
		System.out.println("These are exsting tickets/cards:");
		System.out.println("abcdefgh: One-way ticket between Chatelet and Olympiades: New – 3.42 euro");
		System.out.println("ijklmnop: 24h tickets: New");
		System.out.println("ABCDEFGH: Prepaid card: 5.65 euros");
		System.out.print("Please provide the ticket code you want to enter/exit: ");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		return input;
	}
	
	public static int checkPseudoBarCode(String pseudoBarCode) {
		int checkNumber = 1;
		for(int i = 0; i < pseudoBarCode.length(); i++) {
			if (pseudoBarCode.charAt(i) > '1' && pseudoBarCode.charAt(i) < '9') {
				checkNumber = -1;
				break;
			}
		}
		if( pseudoBarCode.length() != 8 || checkNumber == -1) {
			return -1; // errors length or number
		} else {
			if( pseudoBarCode.equals(pseudoBarCode.toUpperCase()) ) {
				return 1; // 1 la prepaid card
			} else if ( pseudoBarCode.equals(pseudoBarCode.toLowerCase()) ) {
				return 2; // 0 la ticket
			} else {
				return 0; // errors
			}
		}
	}
	public static void main(String[] args) throws InvalidIDException, ClassNotFoundException, SQLException {
		
		int checkEnterExit = 0, stationID=0;
		boolean checkInput, checkStation;
		String  station, pseudoBarCode = null, cardCode = null, ticketCode = null;
		while (true) {
			String inputMainScreen = mainScreen();
			
			// xu ly inputMainScreen
			if(inputMainScreen.length() < 3) {
				System.out.println("Systax Error");
				return;
			}
			checkEnterExit = checkEnterExit(inputMainScreen);
			checkInput = checkInput(inputMainScreen);
			checkStation = checkStation(inputMainScreen);
			if (checkEnterExit == 0 || checkInput == false || checkStation == false) {
				System.out.println("Systax Error");
				return;
			}
//			station = saveStation(inputMainScreen.charAt(2));
			stationID = inputMainScreen.charAt(2)-'a'+1;
			switch (checkEnterExit) {
			case 1:
				pseudoBarCode = choosingATicketCard();
				if( checkPseudoBarCode(pseudoBarCode) == -1) {
					System.out.println("Error lengths or number");
				} else if( checkPseudoBarCode(pseudoBarCode) == 0) {
					System.out.println("Error pseudoBarCode");
				} else if( checkPseudoBarCode(pseudoBarCode) == 1) {
					// handle Card
					cardCode = CardScannerInterface.process(pseudoBarCode);
					PrepaidCard card = new PrepaidCard();
					card = PrepaidCardBUS.getCardByCode(cardCode);
					
					if( card.getId() == null) {
						System.out.println("Card does not exist");
						GateInterface.close();
					} else if( TripBUS.getTripByTicketCardID(card.getId()).size() > 0) {
						System.out.println("Card is using");
					}else if( PrepaidCardBUS.checkBalanceFirst(card.getId())) {
						GateInterface.open();
						PrepaidCardBUS.displayCardInfor(card);
						TripBUS.createNewTrip(card.getId(), stationID);
					}else {
						System.out.println("Invalid Prepaid Card");
						PrepaidCardBUS.displayCardInfor(card);
						System.out.println("Not enough balance: Expected 1.9 euros");
					}
				} else if( checkPseudoBarCode(pseudoBarCode) == 2) {
					// handle Ticket
					System.out.println("Ticket");
				}
				pressAnyKeyToContinue();
				break;
			case 2:
				System.out.println("Exit-Station: " + stationID);
				pseudoBarCode = choosingATicketCard();
				System.out.println(pseudoBarCode);
				// Todo
				if( checkPseudoBarCode(pseudoBarCode) == -1) {
					System.out.println("Error lengths or number");
				} else if( checkPseudoBarCode(pseudoBarCode) == 0) {
					System.out.println("Error pseudoBarCode");
				} else if( checkPseudoBarCode(pseudoBarCode) == 1) {
					// handle Card
					cardCode = CardScannerInterface.process(pseudoBarCode);
					System.out.println(cardCode);
					PrepaidCard card = new PrepaidCard();
					card = PrepaidCardBUS.getCardByCode(cardCode);
					
					if( card.getId() == null) {
						System.out.println("Card does not exist");
						GateInterface.close();
					} else if( TripBUS.getTripByTicketCardID(card.getId()).size() != 1) {
						System.out.println("Card chua duoc quet khi vao");
					}else {
						System.out.println("Hello");
//						System.out.println(stationID);
						Trip trip = TripBUS.getTripByTicketCardID(card.getId()).get(0);
						double fee = PrepaidCardBUS.calculateFee(trip.getDepartureStationID(), stationID);
						
//						System.out.println(trip.getId());
//						System.out.println(fee);
//						System.out.println(card.getBalance());
						if( fee > card.getBalance()) {
							System.out.println("Balance nho hon fee, hay nap tien vao card");
						}else {
							PrepaidCardBUS.updateBalance(card.getId(), fee);
							TripBUS.updateTrip(trip.getId(), stationID, fee);
							System.out.println("OK");
							GateInterface.open();
						}
					}
				} else if( checkPseudoBarCode(pseudoBarCode) == 2) {
					// handle Ticket
				}
				pressAnyKeyToContinue();
				break;
			default:
				System.out.println("Systax Error");
				break;
			}
		}
		
	}
}
