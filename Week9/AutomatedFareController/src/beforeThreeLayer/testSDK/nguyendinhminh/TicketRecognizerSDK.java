package beforeThreeLayer.testSDK.nguyendinhminh;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;

public class TicketRecognizerSDK {
    public static void main(String[] args) throws InvalidIDException {
        String pseudoBarCode = "abcdefgh";
        TicketRecognizer ticketRecognizer = TicketRecognizer.getInstance();
        String ticketId = ticketRecognizer.process(pseudoBarCode);
        System.out.println(ticketId);
    }
}
